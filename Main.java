import java.awt.Color;
import java.awt.Graphics;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JSlider;
import javax.swing.SwingConstants;

public class Main extends JPanel {

    private int x, y, x2, y2;
    private int clickCount;

    private int oneX = 7;
    private int oneY = 7;

    boolean up = false;
    boolean down = true;
    boolean left = false;
    boolean right = true;

    public static void main(String[] args) {
        JFrame f = new JFrame();
        f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        f.setContentPane(new Main());
        f.setSize(600, 600);
        f.setLayout(null);
        f.setLocationRelativeTo(null);
        f.setVisible(true);
        f.setResizable(false);
        System.out.println("click to set first vertex of rectangle");

        JSlider verticalSlider = new JSlider(SwingConstants.VERTICAL, 0, 100, 50);
        verticalSlider.setBounds(570, 0, 20, 570);
        f.add(verticalSlider);

        JSlider horizontalSlider = new JSlider(SwingConstants.HORIZONTAL, 0, 100, 50);
        horizontalSlider.setBounds(0, 550, 570, 20);
        f.add(horizontalSlider);
        //moveIt();

    }

    Main() {
        x = y = x2 = y2 = 0;

        addMouseListener(new MouseListener() {
            @Override
            public void mouseReleased(MouseEvent e) {
            }

            @Override
            public void mousePressed(MouseEvent e) {
            }

            @Override
            public void mouseExited(MouseEvent e) {
            }

            @Override
            public void mouseEntered(MouseEvent e) {
            }

            @Override
            public void mouseClicked(MouseEvent e) {
                if (clickCount == 0) {
                    setStartPoint(e.getX(), e.getY());
                    clickCount++;
                    System.out.println("click one more time to draw rectangle");
                } else if (clickCount == 1) {
                    setEndPoint(e.getX(), e.getY());
                    clickCount = 0;
                    repaint();
                    System.out.println("click to set first vertex of rectangle");
                }
            }
        });
    }

    public void setStartPoint(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public void setEndPoint(int x, int y) {
        x2 = x;
        y2 = y;
    }

    public void paintComponent(Graphics g) {
        super.paintComponent(g);
        g.setColor(Color.GREEN);
        Rectangle r = new Rectangle(g, x, y, x2, y2);
    }

}