import java.awt.Graphics;

public class Rectangle implements Runnable {

    private int verticalSpeed;
    private int horizontalSpeed;

    @Override
    public void run() {
    }

    public Rectangle(Graphics g, int x, int y, int x2, int y2) {
        int px = Math.min(x, x2);
        int py = Math.min(y, y2);
        int pw = Math.abs(x - x2);
        int ph = Math.abs(y - y2);
        g.fillRect(px, py, pw, ph);
    }
}